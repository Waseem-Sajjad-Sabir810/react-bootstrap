import Navbar from "../components/navbar";

import Header from "../components/home/header";
import Services from "../components/home/services";
import Technology from "../components/home/technology";
import Portfolio from "../components/home/portfolio";
import CallUs from "../components/home/call-us";
import Process from "../components/home/process";

import Footer from "../components/footer";


const Home = () => {

    return (
        <div className="Home">
            <Navbar />
            <Header />
            <Services />
            <Technology />
            <Portfolio />
            <CallUs />
            <Process />
            <Footer />
        </div>
    );
}

export default Home;

import { Link } from "react-router-dom";
import { Nav, Navbar, NavDropdown } from "react-bootstrap";

import logo from "../assets/images/logo.png";
import logoSm from "../assets/images/logo2.png";
import mail from "../assets/images/mail.png";
import call from "../assets/images/telephone-call.png";

import { useState, useEffect } from "react";


const MyNavbar = () => {
    const [show, setShow] = useState(false);
    const [stickyScroll, setScroll] = useState(false);
    const [stickyDefault, setSticky] = useState(false);
    const showDropdown = (e) => {
        setShow(!show);
    }
    const hideDropdown = e => {
        setShow(false);
    }
    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.matchMedia("(min-width: 768px)").matches) {

                setScroll(window.scrollY > 120);
            }
        });
        if (window.matchMedia("(max-width: 767px)").matches) {
            setSticky();
        }
    }, [stickyScroll, stickyDefault]);

    return (
        <Navbar className="custom-navbar py-0 d-block " expand="lg">
            <div className="container-lg custom-container px-sm-4 px-lg-2 custom-navbar-upper d-none d-md-block">
                <ul className="list-unstyled mb-0 py-3 d-flex align-items-center">
                    <li className="logo col-3">
                        <img src={logo} alt="logo" className="w-100 h-100 img-contain" />
                    </li>
                    <li className="ms-auto d-inline-flex align-items-center info-text me-2">
                        <img src={mail} alt="email-icon" className="img-contain icons me-1" />
                        <span>
                            info@mangocoders.com
                        </span>
                    </li>
                    <li className="d-inline-flex align-items-center info-text">
                        <img src={call} alt="telephone-call-icon" className="img-contain icons me-1" />
                        <span>
                            +92-306-4500050
                        </span>
                    </li>
                </ul>
            </div>

            <div className={`custom-navbar-lower py-3 bg-primary ${stickyScroll ? "is-sticky" : ""} ${stickyDefault ? "is-sticky" : ""} `} >
                <div className="container-lg custom-container px-sm-4 px-lg-2 d-flex align-items-center position-relative">
                    <Navbar.Toggle aria-controls="basic-navbar-nav" className="bg-white shadow-none border-0" />
                    <div className={`logo-sm mx-auto ${stickyScroll ? "me-xl-5 me-lg-1" : ""}`}>
                        <img src={logoSm} alt="logo" className="w-100 h-100 img-contain" />
                    </div>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Link to="/" className="text-decoration-none active font-15 fw-bold text-black">
                                Home
                            </Link>
                            <Link to="/About" className="text-decoration-none font-15 fw-bold text-black">
                                About
                            </Link>
                            <Link to="/About" className="d-block d-lg-none text-decoration-none font-15 fw-bold text-black">
                                Services
                            </Link>
                            <NavDropdown show={show} className="d-none d-lg-block"
                                onMouseEnter={showDropdown}
                                onMouseLeave={hideDropdown} title="Services" id="basic-nav-dropdown">
                                <NavDropdown.Item className="font-13 text-black fw-500" href="#action/3.1">Web Designing</NavDropdown.Item>
                                <NavDropdown.Item className="font-13 text-black fw-500" href="#action/3.2">Content Writing</NavDropdown.Item>
                                <NavDropdown.Item className="font-13 text-black fw-500" href="#action/3.3">Web Development</NavDropdown.Item>
                                <NavDropdown.Item className="font-13 text-black fw-500" href="#action/3.4">Server Management</NavDropdown.Item>
                                <NavDropdown.Item className="font-13 text-black fw-500" href="#action/3.4">Mobile Development</NavDropdown.Item>
                                <NavDropdown.Item className="font-13 text-black fw-500" href="#action/3.4">E-Commerce</NavDropdown.Item>
                                <NavDropdown.Item className="font-13 text-black fw-500" href="#action/3.4">Ongoing Support</NavDropdown.Item>
                            </NavDropdown>
                            <Link to="/Contact" className="text-decoration-none font-15 fw-bold text-black">
                                Portfolio
                            </Link>
                            <Link to="/Contact" className="text-decoration-none font-15 fw-bold text-black">
                                Career
                            </Link>
                            <Link to="/Contact" className="text-decoration-none font-15 fw-bold text-black">
                                Events
                            </Link>
                            <Link to="/Contact" className="text-decoration-none font-15 fw-bold text-black">
                                Blogs
                            </Link>
                            <Link to="/Contact" className="text-decoration-none font-15 fw-bold text-black">
                                Contact
                            </Link>
                            <Link to="/Contact" className="text-decoration-none font-15 fw-bold text-black m-0">
                                Reviews
                            </Link>
                        </Nav>
                    </Navbar.Collapse>
                    <Nav className="ms-lg-auto flex-row social-icons">
                        <Link to="/" className="rounded-circle d-flex justify-content-center align-items-center">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="#000" xmlns="http://www.w3.org/2000/svg">
                                <path d="M13 0.374999C7.475 1.375 2.575 5.725 0.849999 11.1C-1.525 18.575 1.675 26.55 8.5 30.1C9.9 30.85 12.425 31.75 13.05 31.75C13.15 31.75 13.25 29 13.25 25.625V19.5H11.375H9.5V17.25V15H11.35H13.175L13.325 12.35C13.475 9.05 14.05 7.75 15.85 6.65C16.85 6.025 17.35 5.9 19.125 5.8C20.275 5.75 21.5 5.75 21.85 5.85C22.475 6 22.5 6.075 22.5 8V10H20.75C18.3 10 18 10.325 18 13V15H20.15H22.3L22.125 16.675C21.825 19.65 21.95 19.5 19.85 19.5H18V25.725V31.925L19.125 31.725C22.15 31.15 25.425 29.3 27.65 26.9C33.65 20.45 33.325 10.6 26.925 4.425C24.975 2.55 21.65 0.874999 18.875 0.374999C16.45 -0.0500012 15.425 -0.0500012 13 0.374999Z" fill="black" />
                            </svg>
                        </Link>
                        <Link to="/" className="rounded-circle d-flex justify-content-center align-items-center">
                            <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M20.8467 0.42142C18.0345 1.18842 15.8364 4.5249 15.8364 8.05312C15.8364 10.2774 15.7395 10.3541 12.3131 9.39538C9.5009 8.59002 6.07455 6.40406 3.77954 3.87294C1.67847 1.57193 1.48453 1.61028 1.09664 4.48655C0.83804 6.67251 1.16128 8.12982 2.45425 10.3925C3.48862 12.2333 3.52094 12.3483 2.84214 12.1566C2.42192 12.0799 1.84009 11.8498 1.54917 11.6964C0.28853 11.0828 1.25826 14.9945 2.84214 17.0271C3.327 17.6407 4.29672 18.446 5.00785 18.8295L6.30082 19.5582H4.74926H3.1977L3.68256 20.5936C4.49067 22.4728 6.33314 24.2369 7.94934 24.7355L9.46858 25.1957L8.43421 26.0394C6.88265 27.3433 4.1351 28.3787 1.93706 28.4171C0.223881 28.4554 -0.00238706 28.5705 0.417827 29.0307C0.67642 29.3375 2.06636 30.0662 3.55327 30.6798C10.8262 33.6711 18.2607 31.7536 23.4326 25.5408C26.7943 21.5141 28.5075 17.2572 29.1216 11.3512C29.4449 8.43662 29.5418 8.16817 30.9318 6.40406C31.7399 5.40695 32.3217 4.48655 32.2247 4.37149C32.1601 4.29479 31.7076 4.37149 31.2227 4.6016C29.5742 5.33025 29.2186 5.10015 30.1237 3.91129C30.8671 2.99089 31.6752 1.45688 31.6752 1.03502C31.6752 0.919973 30.8348 1.26513 29.8328 1.76368C27.5377 2.87584 27.4084 2.87584 26.665 1.91708C26.3418 1.49523 25.3397 0.881623 24.467 0.574821C22.6568 -0.0771315 22.6891 -0.0771314 20.8467 0.42142Z" fill="black" />
                            </svg>
                        </Link>
                        <Link to="/" className="rounded-circle d-flex justify-content-center align-items-center me-0">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="#000" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1C0.05 1.95 0 2.65 0 16C0 29.35 0.05 30.05 1 31C1.95 31.95 2.65 32 16 32C29.35 32 30.05 31.95 31 31C31.95 30.05 32 29.35 32 16C32 2.65 31.95 1.95 31 1C30.05 0.0499995 29.35 -4.76837e-07 16 -4.76837e-07C2.65 -4.76837e-07 1.95 0.0499995 1 1ZM9.2 5.3C10.25 6.3 10.2 7.95 9.15 9.1C7.5 10.9 4.5 9.75 4.5 7.25C4.5 4.85 7.5 3.6 9.2 5.3ZM24 13.25C26.6 14.6 27 15.7 27 21.6V27H24.75H22.5V22.7C22.5 17.65 22.05 16.5 20 16.5C17.95 16.5 17.5 17.65 17.5 22.7V27H15.25H13V20V13H15.25C17 13 17.5 13.2 17.5 13.85C17.5 14.55 17.6 14.6 18.4 13.9C20.15 12.35 21.9 12.15 24 13.25ZM9.5 20V27H7.25H5V20V13H7.25H9.5V20Z" fill="black" />
                            </svg>
                        </Link>
                    </Nav>
                </div>
            </div>
        </Navbar>


    );
}

export default MyNavbar;
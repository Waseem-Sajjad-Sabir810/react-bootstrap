
import { Row, Col, Form, Button } from "react-bootstrap";
import '../my-style.module.css';


const homeHeader = () => {
    return (

        <div className="home-header">
            <header className="header">
                <div className="container-lg custom-container ">
                    <Row className=" align-items-center h-100">
                        <Col xs="12" sm="10" md='6' className="mb-3 mb-md-0 py-3 py-md-0 px-3 pe-lg-0 mx-auto">
                            <div className="header-text">
                                <h1 className="mb-3 lh-sm text-primary fw-bold text-center">
                                    Mangocoders Software & Website <br />
                                    Development Company
                                </h1>
                                <p className="text-white lh-6 font-20 fw-light text-center text-lg-start mb-0">
                                    We provide enterprise web development services to assist businesses in streamlining workflows, increasing staff productivity, and improving customer experience
                                </p>
                            </div>
                        </Col>
                        <Col xs="12" sm="10" md="6" className="ps-lg-0 px-3 mx-auto header-form">
                            <Form className="custom-form  bg-white p-4 text-center ms-md-auto">
                                <h4 className="fw-bold text-black">
                                    Get In Contact
                                </h4>
                                <Form.Control className="shadow-none mb-3" type="text" placeholder="Name" />
                                <Form.Control className="shadow-none mb-3" type="email" placeholder="Email" />
                                <Form.Control className="shadow-none mb-3" type="phone" placeholder="Phone Number" />
                                <Form.Control className="shadow-none mb-3" as="textarea" rows={3} placeholder="Message" />
                                <Button variant="primary" type="submit" className="w-100">Send</Button>
                            </Form>
                        </Col>
                    </Row>
                </div>
            </header>
        </div >
    );
}

export default homeHeader;
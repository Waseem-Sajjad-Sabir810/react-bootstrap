import user from "../../assets/images/user.png";
import call from "../../assets/images/phone-call.png";

import { useEffect } from "react";

import CountUp from 'react-countup';

const Callus = () => {

    // const { countUp, start } = useCountUp({
    //     start: 0,
    //     duration: 5,
    //     end: 200,
    //     startOnMount: false
    // })

    useEffect(() => {
        const callUs = document.querySelector('.call-us');
        // const statsTitle = document.querySelector('.call-us .stats-title');
        document.addEventListener('scroll', () => {
            if (callUs.getBoundingClientRect().bottom <= window.innerHeight) {
                // start();
            }
        })
    }, []);

    return (
        <div className="call-us position-relative">
            <div className="container-lg custom-container">
                <div className="row mx-0 h-100 mx-3 mx-lg-5">
                    <div className="col-lg d-none d-lg-block align-self-end pt-3">
                        <img src={user} alt="user" className="call-us-user d-block ms-auto img-contain" />

                    </div>
                    <div className="col-lg text-white text-center align-self-center py-5 pe-lg-5 me-lg-5">
                        <img src={call} alt="" className="img-contain" />
                        <h5 className="font-20 mt-3">
                            CALL US 24/7
                        </h5>
                        <h6 className="d-block font-30 mb-3 text-white">
                            +92-306-4500050
                        </h6>
                        <p className="mb-0 font-18 fw-500 mx-auto pb-5 col-11 col-md-8 col-lg-12 col-xl-10">
                            Have any idea or project for in your mind call us or schedule a appointment. Our representative will reply you shortly.
                        </p>
                    </div>
                </div>
                <div className="row mx-auto  px-0 py-2 p-lg-3 position-absolute stats text-center shadow">
                    <div className="col">
                        <h4 className="stats-title font-48 fw-bold fw-bold text-primary">
                            <CountUp start={0} end={200} prefix="+" />
                        </h4>
                        <h5 className="stats-detail mb-0">
                            Happy Clients
                        </h5>
                    </div>
                    <div className="col">
                        <h4 className="stats-title font-48 fw-bold text-primary">
                            <CountUp start={0} end={100} prefix="+" />
                        </h4>
                        <h5 className="stats-detail mb-0">
                            Companies
                        </h5>
                    </div>
                    <div className="col">
                        <h4 className="stats-title font-48 fw-bold text-primary">
                            <CountUp start={0} end={500} prefix="+" />
                        </h4>
                        <h5 className="stats-detail mb-0">
                            Projects Done
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Callus;
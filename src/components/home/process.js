
import processImg1 from "../../assets/images/circle1.png";
import processImg2 from "../../assets/images/circle2.png";
import processImg3 from "../../assets/images/circle3.png";
import processImg4 from "../../assets/images/circle4.png";

const process = () => {
    return (

        <div className="process text-center mt-3">
            <h3 className=" text-black ">
                Our Working Process - How We Work For Our Customers
            </h3>
            <div className="animated-bar mx-auto mt-3 rounded-pill position-relative">
                <span className="d-block rounded-circle bg-primary position-absolute">
                </span>
            </div>
            <div className="container-lg custom-container">
                <div className="row g-3 row-cols-1 row-cols-md-2 row-cols-lg-4 mt-5 mx-3 mx-lg-5">
                    <div className="col">
                        <div className="process-img position-relative">
                            <img src={processImg1} alt="process-circle-red" className="w-100 h-100 img-contain" />
                            <div className="process-img-text position-absolute start-50 ">
                                <h2 className="fw-light">
                                    01
                                </h2>
                                <h5 className="mb-0">
                                    DIscovery
                                </h5>
                            </div>
                        </div>
                        <p className="mb-0 mt-5 process-text lh-md">
                            The process where understanding of the project is developed in between the client and the company.
                        </p>
                    </div>
                    <div className="col">
                        <div className="process-img position-relative">
                            <img src={processImg2} alt="process-circle-red" className="w-100 h-100 img-contain" />
                            <div className="process-img-text position-absolute start-50 ">
                                <h2 className="fw-light">
                                    02
                                </h2>
                                <h5 className="mb-0">
                                    Planning
                                </h5>
                            </div>
                        </div>
                        <p className="mb-0 mt-5 process-text lh-md">
                            The phase done by the organization to determine project goals and to plan the project.
                        </p>
                    </div>
                    <div className="col">
                        <div className="process-img position-relative">
                            <img src={processImg3} alt="process-circle-red" className="w-100 h-100 img-contain" />
                            <div className="process-img-text position-absolute start-50 ">
                                <h2 className="fw-light">
                                    03
                                </h2>
                                <h5 className="mb-0">
                                    Execute
                                </h5>
                            </div>
                        </div>
                        <p className="mb-0 mt-5 process-text lh-md">
                            Execution is the phase where project is implemented, tested and deployed by team.
                        </p>
                    </div>
                    <div className="col">
                        <div className="process-img position-relative">
                            <img src={processImg4} alt="process-circle-red" className="w-100 h-100 img-contain" />
                            <div className="process-img-text position-absolute start-50 ">
                                <h2 className="fw-light">
                                    04
                                </h2>
                                <h5 className="mb-0">
                                    Deliver
                                </h5>
                            </div>
                        </div>
                        <p className="mb-0 mt-5 process-text lh-md">
                            Deliver is that phase where the project is delivered to the client after completion.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default process;
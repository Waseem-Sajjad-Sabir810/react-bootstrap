
import techno1 from "../../assets/images/android.png";
import techno2 from "../../assets/images/flutter.png";
import techno3 from "../../assets/images/ionic.png";
import techno4 from "../../assets/images/IOS.png";
import techno5 from "../../assets/images/python.png";
import techno6 from "../../assets/images/react-native.png";
import techno7 from "../../assets/images/Mongo-DB.png";
import techno8 from "../../assets/images/my-sql.png";
import techno9 from "../../assets/images/postgresql.png";
import techno10 from "../../assets/images/aws.png";
import techno11 from "../../assets/images/angularjs.png";
import techno12 from "../../assets/images/html.png";
import techno13 from "../../assets/images/bootstrap-5.png";
import techno14 from "../../assets/images/codeigniter.png";
import techno15 from "../../assets/images/laravel.png";
import techno16 from "../../assets/images/php.png";
import techno17 from "../../assets/images/react-JS.png";
import techno18 from "../../assets/images/tailwind.png";
import techno19 from "../../assets/images/vuejs.png";
import techno20 from "../../assets/images/nuxtjs.png";
import techno21 from "../../assets/images/wordpress.png";
import techno22 from "../../assets/images/Cpanel.png";
import techno23 from "../../assets/images/git.png";
import techno24 from "../../assets/images/illustrator.png";
import techno25 from "../../assets/images/photoshop-icon.png";
import techno26 from "../../assets/images/linux-server.png";

import { Nav, Tab } from "react-bootstrap";

const technology = () => {

    return (
        <section className="technology text-center overflow-hidden">
            <h5>
                TECHNOLOGIES
            </h5>
            <h3 className="text-black">
                We Are Offering All Kinds of IT Solutions
                <span className="d-block">
                    Services
                </span>
            </h3>
            <div className="animated-bar mx-auto mt-3 rounded-pill position-relative">
                <span className="d-block rounded-circle bg-primary position-absolute">
                </span>
            </div>
            <Tab.Container id="left-tabs-example" defaultActiveKey="all">
                <Nav className=" mx-3 mx-sm-0 flex-column flex-sm-row mt-4 justify-content-center">
                    <Nav.Link className="text-uppercase fw-500" eventKey="all">All</Nav.Link>
                    <Nav.Link className="text-uppercase fw-500" eventKey="mobile">Mobile</Nav.Link>
                    <Nav.Link className="text-uppercase fw-500" eventKey="web">Web</Nav.Link>
                    <Nav.Link className="text-uppercase fw-500" eventKey="database">Database</Nav.Link>
                    <Nav.Link className="text-uppercase fw-500" eventKey="others">Others</Nav.Link>
                </Nav>
                <div className="container-lg custom-container-sm">
                    <div className="row">
                        <div className="col">
                            <Tab.Content>
                                <Tab.Pane eventKey="all">
                                    <div className="row row-cols-md-5 row-cols-xl-6 g-5 mt-4">
                                        <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno1} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                android
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno2} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                flutter
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno3} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Ionic
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno4} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Ios
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno5} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Python
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno6} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Rreact Native
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-database">
                                            <img src={techno7} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Mongo Db
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-database">
                                            <img src={techno8} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                My Sql
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-database">
                                            <img src={techno9} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Postresql
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-others">
                                            <img src={techno10} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Aws
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno11} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Angular Js
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno12} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Html
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno13} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Bootstrap 5
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno14} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Codeignator
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno15} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Laravel
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno16} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Php
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno17} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                React Js
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno18} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Tailwind
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno19} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Vue Js
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno20} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Nuxt Js
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno21} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Wordpress
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-others">
                                            <img src={techno22} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Cpanel
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-others">
                                            <img src={techno23} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                git
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-others">
                                            <img src={techno24} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Adobe illustrator
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-others">
                                            <img src={techno25} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Adobe Photoshop
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-others">
                                            <img src={techno26} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Linux server
                                            </h6>
                                        </div>
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="mobile">
                                    <div className="row row-cols-md-5 row-cols-xl-6 g-5 mt-4">
                                        <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno2} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                flutter
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno3} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Ionic
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno4} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Ios
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-mobile">
                                            <img src={techno6} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Rreact Native
                                            </h6>
                                        </div>

                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="web">
                                    <div className="row row-cols-md-5 row-cols-xl-6 g-5 mt-4">
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno5} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Python
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno11} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Angular Js
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno12} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Html
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno13} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Bootstrap 5
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno14} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Codeignator
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno15} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Laravel
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno16} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Php
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-web">
                                            <img src={techno17} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                React Js
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno18} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Tailwind
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno19} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Vue Js
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno20} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Nuxt Js
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-web">
                                            <img src={techno21} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Wordpress
                                            </h6>
                                        </div>
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="database">
                                    <div className="row row-cols-md-5 row-cols-xl-6 g-5 mt-4">
                                        <div className="col technology-list-all technology-list-database">
                                            <img src={techno7} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Mongo Db
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-database">
                                            <img src={techno8} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                My Sql
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-database">
                                            <img src={techno9} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Postresql
                                            </h6>
                                        </div>
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="others">
                                    <div className="row row-cols-md-5 row-cols-xl-6 g-5 mt-4">
                                        <div className="col technology-list-all technology-list-others">
                                            <img src={techno10} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Aws
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-others">
                                            <img src={techno22} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Cpanel
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-others">
                                            <img src={techno23} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                git
                                            </h6>
                                        </div> <div className="col technology-list-all technology-list-others">
                                            <img src={techno24} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Adobe illustrator
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-others">
                                            <img src={techno25} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Adobe Photoshop
                                            </h6>
                                        </div>
                                        <div className="col technology-list-all technology-list-others">
                                            <img src={techno26} alt="" className="img-contain" />
                                            <h6 className="mb-0 mt-3 text-black fw-bold font-15">
                                                Linux server
                                            </h6>
                                        </div>
                                    </div>
                                </Tab.Pane>
                            </Tab.Content>
                        </div>
                    </div>
                </div>
            </Tab.Container>
        </section>

    );
}

export default technology;
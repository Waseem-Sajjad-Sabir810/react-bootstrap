import { Carousel } from "react-bootstrap";

import carouselImg1 from "../../assets/images/carsyours.jpg";
import carouselImg2 from "../../assets/images/top-academics.jpg";
import carouselImg3 from "../../assets/images/indyguide-large-scaled.jpg";
// import carouselImg4 from "../../assets/images/affweb-scaled.jpg";
// import carouselImg5 from "../../assets/images/siff-scaled.jpg";

import techno10 from "../../assets/images/aws.png";
import techno13 from "../../assets/images/bootstrap-5.png";
import techno15 from "../../assets/images/laravel.png";

import techno26 from "../../assets/images/linux-server.png";
import laptop from "../../assets/images/laptop.png";


const portfolio = () => {
    return (

        <section className="portfolio bg-light-gray">
            <h3 className=" text-black text-center">
                Portfolio
            </h3>
            <div className="animated-bar mx-auto mt-3 rounded-pill position-relative">
                <span className="d-block rounded-circle bg-primary position-absolute">
                </span>
            </div>
            <div className="container-lg custom-container">
                <div className="row g-0 mt-5 mx-3 mx-lg-5">
                    <div className="col">
                        <Carousel pause={'hover'} indicators={false}>
                            <Carousel.Item>
                                <div className="row g-0">
                                    <div className="col-md">
                                        <div className="img-box position-relative">
                                            <img src={carouselImg1} alt="cars-yours" className="w-100 h-100 img-cover" />
                                        </div>
                                    </div>
                                    <div className="col-md bg-white rounded-6-right">
                                        <div className="page p-3">
                                            <h3 className="page-title text-uppercase text-black mb-2">
                                                Carsyours
                                            </h3>
                                            <p className="page-detail lh-md font-14 mb-0">
                                                A marketplace that connects online vehicle shoppers with your dealership through the most advanced digital retailing website tools and services. We offer custom web solutions that are current, effective, measurable, and easy to maintain.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <Carousel.Caption className="row align-items-center py-3">
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Platform
                                        </h6>
                                        <img src={laptop} alt="" className="img-contain carousel-caption-box-img" />
                                    </div>
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Timeline
                                            <span className="d-block">
                                                06
                                            </span>
                                            <span className="d-block">
                                                Months
                                            </span>
                                        </h6>
                                    </div>
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Team
                                            <span className="d-block">
                                                03
                                            </span>
                                            <span className="d-block">
                                                Members
                                            </span>
                                        </h6>
                                    </div>
                                    <div className="col-6 col-md-3 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Stack
                                        </h6>
                                        <div className="d-flex justify-content-center">
                                            <img src={techno15} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno13} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno26} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno10} alt="" className="img-contain carousel-caption-box-img" />
                                        </div>
                                    </div>
                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item>
                                <div className="row g-0">
                                    <div className="col-md">
                                        <div className="img-box position-relative">
                                            <img src={carouselImg2} alt="cars-yours" className="w-100 h-100 img-cover" />
                                        </div>
                                    </div>
                                    <div className="col-md bg-white rounded-6-right">
                                        <div className="page p-3">
                                            <h3 className="page-title text-uppercase text-black mb-2">
                                                Carsyours
                                            </h3>
                                            <p className="page-detail lh-md font-14 mb-0">
                                                A marketplace that connects online vehicle shoppers with your dealership through the most advanced digital retailing website tools and services. We offer custom web solutions that are current, effective, measurable, and easy to maintain.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <Carousel.Caption className="row align-items-center py-3">
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Platform
                                        </h6>
                                        <img src={laptop} alt="" className="img-contain carousel-caption-box-img" />
                                    </div>
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Timeline
                                            <span className="d-block">
                                                06
                                            </span>
                                            <span className="d-block">
                                                Months
                                            </span>
                                        </h6>
                                    </div>
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Team
                                            <span className="d-block">
                                                03
                                            </span>
                                            <span className="d-block">
                                                Members
                                            </span>
                                        </h6>
                                    </div>
                                    <div className="col-6 col-md-3 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Stack
                                        </h6>
                                        <div className="d-flex justify-content-center">
                                            <img src={techno15} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno13} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno26} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno10} alt="" className="img-contain carousel-caption-box-img" />
                                        </div>
                                    </div>
                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item>
                                <div className="row g-0">
                                    <div className="col-md">
                                        <div className="img-box position-relative">
                                            <img src={carouselImg3} alt="cars-yours" className="w-100 h-100 img-cover" />
                                        </div>
                                    </div>
                                    <div className="col-md bg-white rounded-6-right">
                                        <div className="page p-3">
                                            <h3 className="page-title text-uppercase text-black mb-2">
                                                Carsyours
                                            </h3>
                                            <p className="page-detail lh-md font-14 mb-0">
                                                A marketplace that connects online vehicle shoppers with your dealership through the most advanced digital retailing website tools and services. We offer custom web solutions that are current, effective, measurable, and easy to maintain.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <Carousel.Caption className="row align-items-center py-3">
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Platform
                                        </h6>
                                        <img src={laptop} alt="" className="img-contain carousel-caption-box-img" />
                                    </div>
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Timeline
                                            <span className="d-block">
                                                06
                                            </span>
                                            <span className="d-block">
                                                Months
                                            </span>
                                        </h6>
                                    </div>
                                    <div className="col-6 col-md-3 mb-3 mb-md-0 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Team
                                            <span className="d-block">
                                                03
                                            </span>
                                            <span className="d-block">
                                                Members
                                            </span>
                                        </h6>
                                    </div>
                                    <div className="col-6 col-md-3 carousel-caption-box position-relative">
                                        <h6 className="carousel-caption-box-text">
                                            Stack
                                        </h6>
                                        <div className="d-flex justify-content-center">
                                            <img src={techno15} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno13} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno26} alt="" className="img-contain carousel-caption-box-img me-1" />
                                            <img src={techno10} alt="" className="img-contain carousel-caption-box-img" />
                                        </div>
                                    </div>
                                </Carousel.Caption>
                            </Carousel.Item>
                        </Carousel>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default portfolio;
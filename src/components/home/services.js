import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

import services1 from "../../assets/images/services-icon1.png";
import services2 from "../../assets/images/services-icon2.png";
import services3 from "../../assets/images/services-icon3.png";
import services4 from "../../assets/images/services-icon4.png";
import services5 from "../../assets/images/services-icon5.png";
import services6 from "../../assets/images/services-icon6.png";
import services7 from "../../assets/images/services-icon7.png";
import services8 from "../../assets/images/services-icon8.png";


const Services = () => {

    return (

        <section className="services bg-light-gray mt-3 font-18  text-center">
            <h5>
                SERVICES
            </h5>
            <h3 className=" text-black">
                We Are Increasing Business Success With Technology
            </h3>
            <div className="animated-bar mx-auto mt-3 rounded-pill position-relative">
                <span className="d-block rounded-circle bg-primary position-absolute">
                </span>
            </div>
            <div className="container-lg custom-container-sm overflow-hidden">
                <Row xs={1} sm={2} lg={4} className="g-5 mt-5">
                    <Col>
                        <div className="services-wrapper text-start">
                            <Link to="/" className="d-block text-decoration-none font-21 fw-bold lh-sm services-wrapper-title ps-2 mb-4">
                                Web
                                <span className="d-block">
                                    Designing
                                </span>
                            </Link>
                            <img src={services1} alt="web-designing-icon" className="my-3 d-block mx-sm-auto services-wrapper-icon img-contain" />

                            <p className="mb-0 services-wrapper-text mt-4 font-16 fw-normal lh-md">
                                Web designing directs to the design of the website that has to be displayed on the browsers.
                            </p>
                        </div>
                    </Col>
                    <Col>
                        <div className="services-wrapper text-start">
                            <Link to="/" className="d-block text-decoration-none font-21 fw-bold lh-sm services-wrapper-title ps-2 mb-4">
                                E
                                <span className="d-block">
                                    Commerce
                                </span>
                            </Link>
                            <img src={services2} alt="web-designing-icon" className="my-3 d-block mx-sm-auto services-wrapper-icon img-contain" />

                            <p className="mb-0 services-wrapper-text mt-4 font-16 fw-normal lh-md">
                                We are thought leaders in creating rewarding through responsive web design and emergin UX technologies.
                            </p>
                        </div>
                    </Col>
                    <Col>
                        <div className="services-wrapper text-start">
                            <Link to="/" className="d-block text-decoration-none font-21 fw-bold lh-sm services-wrapper-title ps-2 mb-4">
                                <span className="d-block">
                                    Server
                                </span>
                                Management
                            </Link>
                            <img src={services3} alt="web-designing-icon" className="my-3 d-block mx-sm-auto services-wrapper-icon img-contain" />

                            <p className="mb-0 services-wrapper-text mt-4 font-16 fw-normal lh-md">
                                Focus on your business without worrying about server monitoring server support, server hardening.
                            </p>
                        </div>
                    </Col>
                    <Col>
                        <div className="services-wrapper text-start">
                            <Link to="/" className="d-block text-decoration-none font-21 fw-bold lh-sm services-wrapper-title ps-2 mb-4">
                                <span className="d-block">
                                    Web
                                </span>
                                Develepment
                            </Link>
                            <img src={services4} alt="web-designing-icon" className="my-3 d-block mx-sm-auto services-wrapper-icon img-contain" />

                            <p className="mb-0 services-wrapper-text mt-4 font-16 fw-normal lh-md">
                                Make an online appearance to increase your business with a free maintenance of your presences.
                            </p>
                        </div>
                    </Col>
                    <Col>
                        <div className="services-wrapper text-start">
                            <Link to="/" className="d-block text-decoration-none font-21 fw-bold lh-sm services-wrapper-title ps-2 mb-4">
                                <span className="d-block">
                                    Content
                                </span>
                                Writing
                            </Link>
                            <img src={services5} alt="web-designing-icon" className="my-3 d-block mx-sm-auto services-wrapper-icon img-contain" />

                            <p className="mb-0 services-wrapper-text mt-4 font-16 fw-normal lh-md">
                                Content writing is a complete procedure of organizing, writing and rephrasing a content.
                            </p>
                        </div>
                    </Col>
                    <Col>
                        <div className="services-wrapper text-start">
                            <Link to="/" className="d-block text-decoration-none font-21 fw-bold lh-sm services-wrapper-title ps-2 mb-4">
                                <span className="d-block">
                                    Mobile App
                                </span>
                                Develepment
                            </Link>
                            <img src={services6} alt="web-designing-icon" className="my-3 d-block mx-sm-auto services-wrapper-icon img-contain" />

                            <p className="mb-0 services-wrapper-text mt-4 font-16 fw-normal lh-md">
                                Build Mobile application that improve customer access to your business through the modern technologies.
                            </p>
                        </div>
                    </Col>
                    <Col>
                        <div className="services-wrapper text-start">
                            <Link to="/" className="d-block text-decoration-none font-21 fw-bold lh-sm services-wrapper-title ps-2 mb-4">
                                <span className="d-block">
                                    Product
                                </span>
                                Consulation
                            </Link>
                            <img src={services7} alt="web-designing-icon" className="my-3 d-block mx-sm-auto services-wrapper-icon img-contain" />

                            <p className="mb-0 services-wrapper-text mt-4 font-16 fw-normal lh-md">
                                Build Mobile application that improve customer access to your business through the modern technologies.
                            </p>
                        </div>
                    </Col>
                    <Col>
                        <div className="services-wrapper text-start">
                            <Link to="/" className="d-block text-decoration-none font-21 fw-bold lh-sm services-wrapper-title ps-2 mb-4">
                                <span className="d-block">
                                    Ongoing
                                </span>
                                Support
                            </Link>
                            <img src={services8} alt="web-designing-icon" className="my-3 d-block mx-sm-auto services-wrapper-icon img-contain" />

                            <p className="mb-0 services-wrapper-text mt-4 font-16 fw-normal lh-md">
                                Build Mobile application that improve customer access to your business through the modern technologies.
                            </p>
                        </div>
                    </Col>
                </Row>
            </div>

        </section>

    );
}

export default Services;
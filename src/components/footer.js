import logo from "../assets/images/footer-logo.png";

import map from "../assets/images/map.png";
import mail from "../assets/images/mail.png";
import call from "../assets/images/telephone-call.png";

import { Link } from "react-router-dom";

import { ListGroup } from "react-bootstrap";


const footer = () => {
    return (
        <footer className="footer text-white text-center">
            <div className="container-lg custom-container">
                <div className="row py-5">
                    <div className=" mb-5 mb-lg-0 col-lg">
                        <Link to="/" className="d-block footer-logo mx-auto mx-lg-0 mb-3">
                            <img src={logo} alt="mangocoders-logo" className="w-100 h-100 img-contain" />
                        </Link>
                        <p className="lh-md font-15 text-lg-start col-11 col-md-8 col-lg-12 mx-auto">
                            We provide a wide range of services to fill the voids in your organization or product. We provide you with an online identity to increase your business.
                        </p>
                        <div className="flex-row align-items-center text-lg-start footer-social-icons">
                            <Link to="/" className="rounded-circle d-inline-flex justify-content-center align-items-center me-2">
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13 0.374999C7.475 1.375 2.575 5.725 0.849999 11.1C-1.525 18.575 1.675 26.55 8.5 30.1C9.9 30.85 12.425 31.75 13.05 31.75C13.15 31.75 13.25 29 13.25 25.625V19.5H11.375H9.5V17.25V15H11.35H13.175L13.325 12.35C13.475 9.05 14.05 7.75 15.85 6.65C16.85 6.025 17.35 5.9 19.125 5.8C20.275 5.75 21.5 5.75 21.85 5.85C22.475 6 22.5 6.075 22.5 8V10H20.75C18.3 10 18 10.325 18 13V15H20.15H22.3L22.125 16.675C21.825 19.65 21.95 19.5 19.85 19.5H18V25.725V31.925L19.125 31.725C22.15 31.15 25.425 29.3 27.65 26.9C33.65 20.45 33.325 10.6 26.925 4.425C24.975 2.55 21.65 0.874999 18.875 0.374999C16.45 -0.0500012 15.425 -0.0500012 13 0.374999Z" fill="black" />
                                </svg>
                            </Link>
                            <Link to="/" className="rounded-circle d-inline-flex justify-content-center align-items-center me-2">
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M20.8467 0.42142C18.0345 1.18842 15.8364 4.5249 15.8364 8.05312C15.8364 10.2774 15.7395 10.3541 12.3131 9.39538C9.5009 8.59002 6.07455 6.40406 3.77954 3.87294C1.67847 1.57193 1.48453 1.61028 1.09664 4.48655C0.83804 6.67251 1.16128 8.12982 2.45425 10.3925C3.48862 12.2333 3.52094 12.3483 2.84214 12.1566C2.42192 12.0799 1.84009 11.8498 1.54917 11.6964C0.28853 11.0828 1.25826 14.9945 2.84214 17.0271C3.327 17.6407 4.29672 18.446 5.00785 18.8295L6.30082 19.5582H4.74926H3.1977L3.68256 20.5936C4.49067 22.4728 6.33314 24.2369 7.94934 24.7355L9.46858 25.1957L8.43421 26.0394C6.88265 27.3433 4.1351 28.3787 1.93706 28.4171C0.223881 28.4554 -0.00238706 28.5705 0.417827 29.0307C0.67642 29.3375 2.06636 30.0662 3.55327 30.6798C10.8262 33.6711 18.2607 31.7536 23.4326 25.5408C26.7943 21.5141 28.5075 17.2572 29.1216 11.3512C29.4449 8.43662 29.5418 8.16817 30.9318 6.40406C31.7399 5.40695 32.3217 4.48655 32.2247 4.37149C32.1601 4.29479 31.7076 4.37149 31.2227 4.6016C29.5742 5.33025 29.2186 5.10015 30.1237 3.91129C30.8671 2.99089 31.6752 1.45688 31.6752 1.03502C31.6752 0.919973 30.8348 1.26513 29.8328 1.76368C27.5377 2.87584 27.4084 2.87584 26.665 1.91708C26.3418 1.49523 25.3397 0.881623 24.467 0.574821C22.6568 -0.0771315 22.6891 -0.0771314 20.8467 0.42142Z" fill="black" />
                                </svg>
                            </Link>
                            <Link to="/" className="rounded-circle d-inline-flex justify-content-center align-items-center me-0">
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1C0.05 1.95 0 2.65 0 16C0 29.35 0.05 30.05 1 31C1.95 31.95 2.65 32 16 32C29.35 32 30.05 31.95 31 31C31.95 30.05 32 29.35 32 16C32 2.65 31.95 1.95 31 1C30.05 0.0499995 29.35 -4.76837e-07 16 -4.76837e-07C2.65 -4.76837e-07 1.95 0.0499995 1 1ZM9.2 5.3C10.25 6.3 10.2 7.95 9.15 9.1C7.5 10.9 4.5 9.75 4.5 7.25C4.5 4.85 7.5 3.6 9.2 5.3ZM24 13.25C26.6 14.6 27 15.7 27 21.6V27H24.75H22.5V22.7C22.5 17.65 22.05 16.5 20 16.5C17.95 16.5 17.5 17.65 17.5 22.7V27H15.25H13V20V13H15.25C17 13 17.5 13.2 17.5 13.85C17.5 14.55 17.6 14.6 18.4 13.9C20.15 12.35 21.9 12.15 24 13.25ZM9.5 20V27H7.25H5V20V13H7.25H9.5V20Z" fill="black" />
                                </svg>

                            </Link>
                        </div>
                    </div>
                    <div className="col-lg mb-5 mb-lg-0">
                        <h3 className="mb-4 fw-bold">
                            Servces
                        </h3>
                        <ListGroup as="ul" className="list-unstyled mb-0">
                            <ListGroup.Item as="li" className="mb-3">
                                <Link to="/" className="text-decoration-none">
                                    Web Designing
                                </Link>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="mb-3" >
                                <Link to="/" className="text-decoration-none">
                                    Mobile Development
                                </Link>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="mb-3" >
                                <Link to="/" className="text-decoration-none">
                                    Server Management
                                </Link>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="mb-3" >
                                <Link to="/" className="text-decoration-none">
                                    Web Development
                                </Link>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="mb-3" >
                                <Link to="/" className="text-decoration-none">
                                    Mobile Development
                                </Link>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="mb-3" >
                                <Link to="/" className="text-decoration-none">
                                    Content Writing
                                </Link>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="mb-3" >
                                <Link to="/" className="text-decoration-none">
                                    E Commerce
                                </Link>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="mb-3" >
                                <Link to="/" className="text-decoration-none">
                                    Product Consulation
                                </Link>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" >
                                <Link to="/" className="text-decoration-none">
                                    Ongoing Support
                                </Link>
                            </ListGroup.Item>
                        </ListGroup>
                    </div>
                    <div className="col-lg mb-5 mb-lg-0">
                        <h3 className="mb-4 fw-bold">
                            Head Office
                        </h3>
                        <ListGroup as="ul" className="list-unstyled mb-0 ">
                            <ListGroup.Item as="li" className="d-lg-flex mb-3">
                                <img src={map} alt="map-icon" className="footer-info-icons me-lg-1 mb-1 mb-lg-0 img-contain" />
                                <p className="mb-0 w-100">
                                    5033 Foursprings Ave Mississauga, ON, L5R 0G6
                                </p>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="d-lg-flex mb-3">
                                <img src={call} alt="mail-icon" className="footer-info-icons me-lg-1 mb-1 mb-lg-0 img-contain" />
                                <a href="tel:+1(647)7815782" className="footer-info-links text-decoration-none w-100">
                                    +1 (647) 781-5782
                                </a>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="d-lg-flex mb-3">
                                <img src={mail} alt="call-icon" className="footer-info-icons me-lg-1 mb-1 mb-lg-0 img-contain" />
                                <Link to="/" className="footer-info-links text-decoration-none w-100">
                                    info@mangocoders.com
                                </Link>
                            </ListGroup.Item>
                        </ListGroup>
                    </div>
                    <div className="col-lg">
                        <h3 className="mb-4 fw-bold">
                            Branch Office
                        </h3>
                        <ListGroup as="ul" className="list-unstyled mb-0 ">
                            <ListGroup.Item as="li" className="d-lg-flex mb-3">
                                <img src={map} alt="map-icon" className="footer-info-icons me-lg-1 mb-1 mb-lg-0 img-contain" />
                                <p className="mb-0 w-100">
                                    S-3 3rd Floor, Rakshanda Heights Multan Road Lahore
                                </p>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="d-lg-flex mb-3">
                                <img src={call} alt="mail-icon" className="footer-info-icons me-lg-1 mb-1 mb-lg-0 img-contain" />
                                <a href="tel:+1(647)7815782" className="footer-info-links text-decoration-none w-100">
                                    +92-306-4500050
                                </a>
                            </ListGroup.Item>
                            <ListGroup.Item as="li" className="d-lg-flex mb-3">
                                <img src={mail} alt="call-icon" className="footer-info-icons me-lg-1 mb-1 mb-lg-0 img-contain" />
                                <Link to="/" className="footer-info-links text-decoration-none w-100">
                                    info@mangocoders.com
                                </Link>
                            </ListGroup.Item>
                        </ListGroup>
                    </div>
                </div>
            </div>
            <p className=" py-3 bg-footer-copyright mb-0">
                © 2021 All Rights Reserved. Design & Developed By MangoCoders
            </p>
        </footer>
    );
}

export default footer;
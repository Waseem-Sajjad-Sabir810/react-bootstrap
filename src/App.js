import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// pages 
import Home from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";
function App() {
  return (
    <Router >
      <div className="App">
        <div className="content">
          <Routes>
            <Route path="/" element={<Home />}></Route>
            <Route path="About" element={<About />}></Route>
            <Route path="Contact" element={<Contact />}></Route>
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
